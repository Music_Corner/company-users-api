import fs from 'fs';
import path from 'path';

import mysqlConnection from '../src/services/mysqlConnection';

const DUMP_PATH = path.resolve(__dirname, 'dump.sql');

const migrate = async () => {
  console.log('Started migrating DB from', DUMP_PATH);

  fs.readFile(DUMP_PATH, async (err, data) => {
    try {
			if (err) throw err;

			const queryString = data.toString('ascii');

      await mysqlConnection.query(queryString);
      console.log('DB migrated successfully!');
      mysqlConnection.end();

      return { data: 'success' };
    } catch (error) {
      console.error('DB Migration error', error);
    }
  });
};

migrate();