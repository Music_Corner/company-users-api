import expressServer from './src/common/utils/express/initExpressServer';
import { initRouting } from './src/routes';

expressServer.listen(8081);

initRouting();

console.log('Server is running on port 8081');
