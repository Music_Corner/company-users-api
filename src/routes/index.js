import initAuthRouting from './auth';
import initCompaniesRouting from './companies';
import { initCompanyUsersRoute } from './company-users';

export const initRouting = () => {
	initAuthRouting();
	initCompaniesRouting();
	initCompanyUsersRoute();
};
