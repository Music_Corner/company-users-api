import { API_METHODS } from '../../common/constants/urls';
import expressServer from '../../common/utils/express/initExpressServer';
import { errorHandledWithAuthorization } from '../../common/utils/express/errorHandledWithAuthorization';
import CompaniesController from '../../controllers/companies';

const initCompaniesRouting = () => {
	const companiesController = new CompaniesController();

	expressServer.get(API_METHODS.COMPANIES, errorHandledWithAuthorization(async (req, res) => {
		const companies = await companiesController.getCompanies(req, res);

		res.send({ status: 200, data: companies });
	}));

	expressServer.get(API_METHODS.COMPANY, errorHandledWithAuthorization(async (req, res) => {
		const company = await companiesController.getCompany(req, res);

		res.send({ status: 200, data: company });
	}));

	expressServer.post(API_METHODS.COMPANIES, errorHandledWithAuthorization(async (req, res) => {
		await companiesController.createCompany(req, res);

		res.send({ status: 200 });
	}));

	expressServer.put(API_METHODS.COMPANY, errorHandledWithAuthorization(async (req, res) => {
		await companiesController.updateCompany(req, res);

		res.send({ status: 200 });
	}));

	expressServer.delete(API_METHODS.COMPANY, errorHandledWithAuthorization(async (req, res) => {
		await companiesController.removeCompany(req, res);

		res.send({ status: 200 });
	}));
}

export default initCompaniesRouting;
