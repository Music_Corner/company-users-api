import { API_METHODS } from '../../common/constants/urls';
import { errorHandledWithAuthorization } from '../../common/utils/express/errorHandledWithAuthorization'
import expressServer from '../../common/utils/express/initExpressServer'
import CompanyUsersController from '../../controllers/company-users';

export const initCompanyUsersRoute = () => {
	const companyUsersController = new CompanyUsersController();

	expressServer.get(API_METHODS.COMPANY_ADMINS, errorHandledWithAuthorization(async (req, res) => {
		const companyUsers = await companyUsersController.getCompanyUsers(req, res);

		res.send({ status: 200, data: companyUsers });
	}));

	expressServer.get(API_METHODS.COMPANY_ADMIN, errorHandledWithAuthorization(async (req, res) => {
		const companyUser = await companyUsersController.getCompanyUser(req, res);

		res.send({ status: 200, data: companyUser });
	}));

	expressServer.post(API_METHODS.COMPANY_ADMINS, errorHandledWithAuthorization(async (req, res) => {
		await companyUsersController.createCompanyUser(req, res);

		res.send({ status: 200 });
	}));

	expressServer.put(API_METHODS.COMPANY_ADMIN, errorHandledWithAuthorization(async (req, res) => {
		await companyUsersController.updateCompanyUser(req, res);

		res.send({ status: 200 });
	}));

	expressServer.delete(API_METHODS.COMPANY_ADMIN, errorHandledWithAuthorization(async (req, res) => {
		const companyUser = await companyUsersController.deleteCompanyUser(req, res);

		res.send({ status: 200, data: companyUser });
	}));
}