import { API_METHODS } from '../../common/constants/urls';
import { errorHandled } from '../../common/utils/express/errorHanlded';
import expressServer from '../../common/utils/express/initExpressServer';
import UsersController from '../../controllers/users';

const initAuthRouting = () => {
	const usersController = new UsersController();

	expressServer.post(API_METHODS.REGISTER, errorHandled(async (req, res) => {
		const result = await usersController.registerUser(req, res);

		res.send({ status: 200, ...(result || {}) });
	}));

	expressServer.post(API_METHODS.LOGIN, errorHandled(async (req, res) => {
		const data = await usersController.loginUser(req, res);

		res.send({ status: 200, data });
	}));
};

export default initAuthRouting;
