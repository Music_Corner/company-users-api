import { compare, hash } from 'bcrypt';

import { DEFAULT_OFFSET } from '../constants/mysql';

export const getOffset = (page = 0, limit = DEFAULT_OFFSET) => page * limit;

export const hashPassword = password => hash(password, 8);
