import { errorHandled } from './errorHanlded';
import { withAuthorizationCheck } from './withAuthorizationCheck';

export const errorHandledWithAuthorization = callBack => async (req, res) => {
	const error = await errorHandled(withAuthorizationCheck(callBack))(req, res);
};
