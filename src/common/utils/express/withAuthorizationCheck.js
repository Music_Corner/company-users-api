import jwt from 'jsonwebtoken';

import { HEADERS, STATUSES } from '../../constants/api';
import { ENV } from '../../constants/env';
import { API_ERROR_CODES, ERROR_MESSAGES } from '../../constants/errors';

export const withAuthorizationCheck = (callBack) => (req, res) => new Promise((resolve, reject) => {
	jwt.verify((req.headers[HEADERS.AUTHORZATION] || '').replace('Bearer ', ''), ENV.JWT_SECRET, (err, decoded) => {
		if (!decoded || !decoded.id || err) {
			const status = STATUSES.AUTHORIZATION_ERROR;

			res.status(status).send({
				status,
				message: ERROR_MESSAGES[status],
				code: API_ERROR_CODES.SECURITY.MAIN,
				errors: [{ code: API_ERROR_CODES.SECURITY.INVALID_ACCESS, field: 'accessToken' }],
			});

			return;
		}

		resolve(callBack(req, res));
	});
});
