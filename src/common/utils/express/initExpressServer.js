import express from 'express';
import cors from 'cors';

const expressServer = express();

expressServer.use(cors());

expressServer.use(express.json());
expressServer.use(express.urlencoded());

export default expressServer;
