export const ENV = {
	DEV_ENV: 'development',
	PROD_ENV: 'production',
	IS_DEV() {
		return process.env.NODE_ENV === this.DEV_ENV;
	},
	JWT_SECRET: '06176FCD584EFF5847589532682EA2D674FC609213605D34CCFAE22E77FC2112',
	ACCESS_EXPIRATION: 1,
};
