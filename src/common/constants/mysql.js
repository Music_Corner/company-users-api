export const MYSQL_CONFIG = {
	host: '0.0.0.0',
	port: 3306,
	user: 'root',
	password: 'password',
	database: 'company-users',
	connectionLimit: 10,
	multipleStatements: true,
};

export const DB_NAMES = {
	TABLES: {
		COMPANIES: 'companies',
		USERS: 'users',
		USERS_INFO: 'users_info',
	},
	COLUMNS: {
		ID: 'id',
		NAME: 'name',
		NOTES: 'notes',
		WEBSITE: 'website',
		WEBSITE_PREFIX: 'website_prefix',
		CREATED_AT: 'created_at',
		UPDATED_AT: 'updated_at',
		LICENCE_NUMBER: 'licence_number',
		LICENCE_PREFIX: 'licence_prefix',
		FAX: 'fax',
		ADDRESS: 'address',
		SUBTITLE: 'subtitle',

		IS_REMOVED: 'is_removed',


		COMPANY_ID: 'company_id',
		USER_ID: 'user_id',

		EMAIL: 'email',
		PASSWORD: 'password',

		PHONE: 'phone',
		FIRST_NAME: 'first_name',
		LAST_NAME: 'last_name',
	},
};

export const DEFAULT_OFFSET = 30;
