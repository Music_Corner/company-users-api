export const API_METHODS = {
	AUTH: '/auth',
	get REGISTER() {
		return `${this.AUTH}/register`;
	},
	get LOGIN() {
		return `${this.AUTH}/token`;
	},
	USERS: '/users',
	COMPANIES: '/companies',
	get COMPANY() {
		return `${this.COMPANIES}/:id`;
	},
	get COMPANY_ADMINS() {
		return `${this.COMPANY}/admins`;
	},
	get COMPANY_ADMIN() {
		return `${this.COMPANY_ADMINS}/:adminId`;
	},
	TOKEN: '/token',
	REFRESH_TOKEN: '/refresh',
};
