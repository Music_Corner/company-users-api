export const STATUSES = {
	SUCCESS: 200,
	AUTHORIZATION_ERROR: 401,
	VALIDATION_ERROR_STATUS: 400,
	UNHANDLED_ERROR: 500,
	NOT_FOUND: 404,
	UNPROCESSABLE_ENTITY: 422,
};

export const HEADERS = {
	AUTHORZATION: 'authorization',
};
