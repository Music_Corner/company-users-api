import Model from '..';
import CompanyUserModel from './CompanyUserModel';

export class CompanyUsersModel extends Model {
	constructor(users = []) {
		super(users);

		return users.map(user => new CompanyUserModel(user));
	}
}