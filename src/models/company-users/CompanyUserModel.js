import Model from '..';

export default class CompanyUserModel extends Model {
	constructor(user) {
		super(user);

		const {
			first_name,
			last_name,
			email,
			id
		} = user;

		return this.normalizeToFront({ first_name, last_name, email, id });
	}

	static validate(user) {
		return user;
	}
}