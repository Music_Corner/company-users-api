import Model from '..';

export default class CompanyModel extends Model {
	constructor(company) {
		const {
			id,
			name = '',
			description = '',
			logo = '',
			subdomain = '',
		} = company;

		super(company);

		return this.normalizeToFront(company);
	}

	static validate(company) {
		const validationMap = {
			name: {
				isRequired: true,
				maxLength: 30,
				minLength: 3,
			},
			description: {
				maxLength: 250,
			},
		};

		const error = this.getModelError(validationMap, company);

		if (error) throw error;

		return company;
	}
}
