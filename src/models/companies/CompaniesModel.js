import CompanyModel from './CompanyModel';

export default class CompaniesModel {
	constructor(companies = []) {
		return companies.map(company => new CompanyModel(company));
	}
};
