import { Controller } from '..';
import CompaniesService from '../../services/companies';

export default class CompaniesController extends Controller {
	companiesService = new CompaniesService();

	async getCompanies(req, res) {
		const { page, limit } = this.getParsedQueryParams(req, res);

		const items = await this.companiesService.getCompanies(page, limit);

		return { items };
	}

	async getCompany(req, res) {
		const { id } = this.getParsedRouteParams(req, res);

		const company = await this.companiesService.getCompany(id);

		return company;
	}

	async createCompany(req, res) {
		const form = this.getParsedBody(req, res);

		await this.companiesService.createCompany(form);

		return res;
	}

	async updateCompany(req, res) {
		const form = this.getParsedBody(req, res);
		const { id } = this.getParsedRouteParams(req, res);

		await this.companiesService.updateCompany(id, form);
	}

	async removeCompany(req, res) {
		const { id } = this.getParsedRouteParams(req, res);

		await this.companiesService.removeCompany(id);
	}
}