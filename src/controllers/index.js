export class Controller {
	/**
	 * 
	 * @param {Express req} req 
	 * @param {Express res} res 
	 */
	getParsedBody(req, res) {
		return req.body
	}

	/**
	 * 
	 * @param {Express req} req 
	 * @param {Express res} res 
	 */
	getParsedQueryParams(req, res) {
		return req.query;
	}

	getParsedRouteParams(req, res) {
		return req.params;
	}
}