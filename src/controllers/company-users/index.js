import { Controller } from '..';
import CompanyUsersService from '../../services/company-users';
import UsersService from '../../services/users';

export default class CompanyUsersController extends Controller {
	companyUsersService = new CompanyUsersService();
	usersService = new UsersService();

	async getCompanyUsers(req, res) {
		const { id: companyId } = this.getParsedRouteParams(req, res);

		const users = await this.companyUsersService.getCompanyUsers(companyId);

		return { items: users };
	}

	async getCompanyUser(req, res) {
		const { adminId } = this.getParsedRouteParams(req, res);

		const user = await this.companyUsersService.getCompanyUser(adminId);

		return user;
	}

	async createCompanyUser(req, res) {
		const form = this.getParsedBody(req, res)
		const { id: companyId } = this.getParsedRouteParams(req, res);

		await this.usersService.createUser({ ...form, companyId })
	}

	async updateCompanyUser(req, res) {
		const { adminId } = this.getParsedRouteParams(req);
		const form = this.getParsedBody(req);

		await this.usersService.updatePublicUserInfo(form, adminId);
	}

	async deleteCompanyUser(req, res) {
		const { adminId } = this.getParsedRouteParams(req, res);

		const user = await this.companyUsersService.deleteCompanyUser(adminId);

		return user;
	}
}
