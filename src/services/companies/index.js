import Service from '..';
import { DB_NAMES } from '../../common/constants/mysql';
import CompaniesModel from '../../models/companies/CompaniesModel';
import CompanyModel from '../../models/companies/CompanyModel';
import { createCompany } from './createCompany';
import { updateCompany } from './updateCompany';

export default class CompaniesService extends Service {
	constructor() {
		super();
	}

	/**
	 * 
	 * @param {Number} page 
	 * @param {Number} limitOperator 
	 */
	async getCompanies(page, limit) {
		const items = await this.getInstances(DB_NAMES.TABLES.COMPANIES, page, limit);
		const companies = await new CompaniesModel(items);

		return companies;
	}

	/**
	 * 
	 * @param {Number} id 
	 */
	async getCompany(id) {
		// const [result] = await getCompany(id);

		// const company = new CompanyModel(result);
		// return company;

		const res = await this.getSingleInstanceByColumn(DB_NAMES.TABLES.COMPANIES, DB_NAMES.COLUMNS.ID, id);

		return new CompanyModel(res);
	}

	/**
	 * 
	 * @param {CompanyModel} companyForm 
	 */
	async createCompany(companyForm = {}) {
		const validForm = CompanyModel.validate(companyForm);

		if (validForm) {
			return createCompany(validForm);
		}
	}

	async updateCompany(id, companyForm = {}) {
		const validForm = CompanyModel.validate(companyForm);

		if (validForm) {
			await updateCompany(id, validForm);
		}
	}

	async removeCompany(id) {
		await this.removeInstanceSoftly(DB_NAMES.TABLES.COMPANIES, id);
	}
}