import { DB_NAMES } from '../../common/constants/mysql';
import mysqlConnection from '../mysqlConnection'

/* TODO:
	replace code above with reduce of form object
	and use it in all update methods
*/

export const updateCompany = async (id, form) => {
	const {
		name,
		subtitle,
		website,
		websitePrefix,
		licenceNumber,
		licencePrefix,
		phone,
		fax,
		address,
		notes,
	} = form;

	try {
		await mysqlConnection.query(`
			UPDATE ${DB_NAMES.TABLES.COMPANIES}
			SET
			${DB_NAMES.COLUMNS.NAME} = "${name}",
			${DB_NAMES.COLUMNS.SUBTITLE} = "${subtitle}",
			${DB_NAMES.COLUMNS.WEBSITE} = "${website}",
			${DB_NAMES.COLUMNS.WEBSITE_PREFIX} = "${websitePrefix}",
			${DB_NAMES.COLUMNS.LICENCE_NUMBER} = "${licenceNumber}",
			${DB_NAMES.COLUMNS.LICENCE_PREFIX} = "${licencePrefix}",
			${DB_NAMES.COLUMNS.PHONE} = "${phone}",
			${DB_NAMES.COLUMNS.FAX} = "${fax}",
			${DB_NAMES.COLUMNS.ADDRESS} = "${address}",
			${DB_NAMES.COLUMNS.NOTES} = "${notes}"
			WHERE ${DB_NAMES.COLUMNS.ID} = ${id}
		`);

		return
	} catch (error) {
		throw error;
	}
};
