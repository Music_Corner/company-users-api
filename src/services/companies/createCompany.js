import { DB_NAMES } from '../../common/constants/mysql';
import mysqlConnection from '../mysqlConnection'

export const createCompany = async (form) => {
	const {
		name,
		subtitle,
		website,
		websitePrefix,
		licenceNumber,
		licencePrefix,
		phone,
		fax,
		address,
		notes,
	} = form;

	try {
		await mysqlConnection.query(`
			INSERT INTO ${DB_NAMES.TABLES.COMPANIES}
			(
				${DB_NAMES.COLUMNS.NAME}, ${DB_NAMES.COLUMNS.NOTES}, ${DB_NAMES.COLUMNS.WEBSITE}, ${DB_NAMES.COLUMNS.WEBSITE_PREFIX},
				${DB_NAMES.COLUMNS.SUBTITLE}, ${DB_NAMES.COLUMNS.LICENCE_NUMBER}, ${DB_NAMES.COLUMNS.LICENCE_PREFIX}, ${DB_NAMES.COLUMNS.PHONE},
				${DB_NAMES.COLUMNS.FAX}, ${DB_NAMES.COLUMNS.ADDRESS}
			)
			VALUES (
				"${name}", "${notes}", "${website}", "${websitePrefix}",
				"${subtitle}", "${licenceNumber}", "${licencePrefix}", "${phone}",
				"${fax}", "${address}"
			)
		`);

		return
	} catch (error) {
		throw error;
	}
};
