import { DB_NAMES } from '../../common/constants/mysql';
import mysqlConnection from '../mysqlConnection'

export const getCompanyUsers = async (companyId) => {
	const [users] = await mysqlConnection.query(`
		SELECT
		users.${DB_NAMES.COLUMNS.EMAIL}, users.${DB_NAMES.COLUMNS.ID},
		info.${DB_NAMES.COLUMNS.FIRST_NAME}, info.${DB_NAMES.COLUMNS.LAST_NAME}
		FROM ${DB_NAMES.TABLES.USERS} users
		RIGHT JOIN ${DB_NAMES.TABLES.USERS_INFO} info
		ON users.${DB_NAMES.COLUMNS.ID} = info.${DB_NAMES.COLUMNS.USER_ID}
		WHERE users.${DB_NAMES.COLUMNS.COMPANY_ID} = ${companyId}
		AND ${DB_NAMES.COLUMNS.IS_REMOVED} = 0
	`);

	return users;
}