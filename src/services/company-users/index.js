import Service from '..';
import { DB_NAMES } from '../../common/constants/mysql';
import CompanyUserModel from '../../models/company-users/CompanyUserModel';
import { CompanyUsersModel } from '../../models/company-users/CompanyUsersModel';
import { getCompanyUser } from './getCompanyUser';
import { getCompanyUsers } from './getCompanyUsers';

export default class CompanyUsersService extends Service {
	async getCompanyUsers(companyId) {
		const users = await getCompanyUsers(companyId);

		return new CompanyUsersModel(users);
	}

	async getCompanyUser(userId) {
		const users = await getCompanyUser(userId);

		return new CompanyUserModel(users);
	}

	async deleteCompanyUser(userId) {
		await this.removeInstanceSoftly(DB_NAMES.TABLES.USERS, userId);
	}
}
