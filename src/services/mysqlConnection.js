import mysql from 'mysql2';
import { MYSQL_CONFIG } from '../common/constants/mysql';

const mysqlConnection = mysql.createPool(MYSQL_CONFIG).promise();

export default mysqlConnection;
