import { GENERATED_ERRORS } from '../common/constants/errors';
import { DB_NAMES } from '../common/constants/mysql';
import { getOffset } from '../common/utils/helpers';
import mysqlConnection from './mysqlConnection';

export default class Service {
	async removeInstanceSoftly(tableName, id) {
		await mysqlConnection.query(`
		UPDATE ${tableName}
		SET ${DB_NAMES.COLUMNS.IS_REMOVED} = 1
		WHERE ${DB_NAMES.COLUMNS.ID} = ${id}
	`)
	}

	async getSingleInstanceByColumn(tableName, columnName, value) {
		const _value = typeof value === 'number' ? _value : `"${value}"`;

		const [[result]] = await mysqlConnection.query(`
			SELECT * FROM ${tableName}
			WHERE ${columnName} = ${_value}
			AND ${DB_NAMES.COLUMNS.IS_REMOVED} = 0
		`);

		if (!result) {
			throw GENERATED_ERRORS.UNPROCESSABLE_ENTITY
		}

		return result;
	}

	async getInstances(tableName, page, limit) {
		const offset = getOffset(page, limit);
		const limitOperator = limit ? `LIMIT ${offset}, ${limit}` : '';

		const [items] = await mysqlConnection.query(`
			SELECT * FROM ${tableName}
			${limitOperator}
			WHERE ${DB_NAMES.COLUMNS.IS_REMOVED} = 0
		`);

		return items;
	}
}
