import bcrypt from 'bcrypt';
import { ERROR_MESSAGES } from '../../common/constants/errors';

import { DB_NAMES } from '../../common/constants/mysql';
import { hashPassword } from '../../common/utils/helpers';
import mysqlConnection from '../mysqlConnection'

export const createUser = async ({ email, password, phone, firstName, lastName, companyId }) => {
	const hashedPassword = await hashPassword(password);

	try {
		const res = await mysqlConnection.query(`
		START TRANSACTION;

		INSERT INTO ${DB_NAMES.TABLES.USERS}
		(${DB_NAMES.COLUMNS.EMAIL}, ${DB_NAMES.COLUMNS.PASSWORD}, ${DB_NAMES.COLUMNS.COMPANY_ID})
		VALUES ("${email}", "${hashedPassword}", ${companyId});
		
		INSERT INTO ${DB_NAMES.TABLES.USERS_INFO}
		(${DB_NAMES.COLUMNS.USER_ID}, ${DB_NAMES.COLUMNS.FIRST_NAME}, ${DB_NAMES.COLUMNS.LAST_NAME}, ${DB_NAMES.COLUMNS.PHONE})
		VALUES (LAST_INSERT_ID(), "${firstName}", "${lastName}", "${phone}");

		COMMIT;
	`);

		return res;
	} catch (err) {
		await mysqlConnection.query('ROLLBACK;');

		const normalizedErr = ERROR_MESSAGES.getMysqlErrMessage(err);
		if (normalizedErr) throw normalizedErr;

		throw err;
	}
};
