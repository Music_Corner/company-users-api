import jwt from 'jsonwebtoken';

import Service from '..';
import { ENV } from '../../common/constants/env';
import Model from '../../models';
import PrivateUserModel from '../../models/users/PrivateUserModel';
import PublicUserModel from '../../models/users/PublicUserModel';

import { createUser } from './createUser';
import { updatePublicUserInfo } from './updatePublicUserInfo';
import { getUserInfo } from './getUserInfo';
import { getUsers } from './getUsers';

export default class UsersService extends Service {
	async createUser(userInfo) {
		const validUserInfo = Model.squashModels(PrivateUserModel, PublicUserModel)(userInfo);

		const result = await createUser(validUserInfo);

		return result;
	}

	async getUsers(column, value) {
		const result = await getUsers(column, value);

		return result;
	}

	async getUserInfo(id) {
		const results = await getUserInfo(id);

		return results;
	}

	async updatePublicUserInfo(form, userId) {
		const publicUserValidForm = PublicUserModel.validate(form);

		await updatePublicUserInfo(publicUserValidForm, userId);
	}

	async generateToken(userData) {
		const date = new Date();

		return jwt.sign(userData, ENV.JWT_SECRET, { expiresIn: date.setDate(date.getDate() + ENV.ACCESS_EXPIRATION) });
	}
}
