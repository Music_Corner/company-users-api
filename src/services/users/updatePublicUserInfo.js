import { DB_NAMES } from '../../common/constants/mysql';
import mysqlConnection from '../mysqlConnection';

export const updatePublicUserInfo = async (user, userId) => {
	const { firstName, lastName } = user;

	await mysqlConnection.query(`
		UPDATE ${DB_NAMES.TABLES.USERS_INFO}
		SET
		${DB_NAMES.COLUMNS.FIRST_NAME} = "${firstName}",
		${DB_NAMES.COLUMNS.LAST_NAME} = "${lastName}"
		WHERE ${DB_NAMES.COLUMNS.USER_ID} = ${userId}
	`);
};
