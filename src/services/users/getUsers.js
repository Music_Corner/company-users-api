import { DB_NAMES } from '../../common/constants/mysql';
import PrivateUserModel from '../../models/users/PrivateUserModel';
import mysqlConnection from '../mysqlConnection';

export const getUsers = async (columnName = DB_NAMES.COLUMNS.ID, value) => {
	const [users = []] = await mysqlConnection.query(`
		SELECT ${DB_NAMES.COLUMNS.ID}, ${DB_NAMES.COLUMNS.PASSWORD}
		FROM ${DB_NAMES.TABLES.USERS}
		WHERE ${columnName} = "${value}"
	`);

	return users.map(user => new PrivateUserModel(user));
};
