import nodeMailer from 'nodemailer';

import Service from '..';

export default class MailSerivce extends Service {
	transporter = nodeMailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'noreply.pager@gmail.com',
			pass: 'passwordForNodeMailer',
		},
	});

	sendMail(email, text, subject) {
		const options = {
			from: 'noreply pager <noreply.pager@gmail.com>',
			to: email,
			subject,
			text,
		};
	
		return new Promise((resolve, reject) => {
			this.transporter.sendMail(options, (error, response) => {
				if (error) {
					reject(error);
				}
	
				resolve(response);
			});
		});
	}
}
